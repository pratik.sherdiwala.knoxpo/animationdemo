package com.example.animationdemo

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.BounceInterpolator
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var animSpinner: Spinner? = null
    private var image: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        animSpinner = findViewById(R.id.spinner)
        image = findViewById(R.id.imageIV)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.anim_options,
            android.R.layout.simple_spinner_item
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        animSpinner!!.adapter = adapter
        animSpinner!!.onItemSelectedListener = this
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (position) {
            1 -> translate()
            2 -> rotate()
            3 -> fade()
            4 -> scale()
            5 -> combineAll()
            6 -> valueAnim()
        }
    }

    private fun translate() {
        ObjectAnimator.ofFloat(image, View.TRANSLATION_Y, 0f, 200f)
            .apply {
                duration = 1000
                interpolator = BounceInterpolator()
                start()
            }
    }

    private fun rotate() {
        ObjectAnimator.ofFloat(image, View.ROTATION, -360f, 0f).apply {
            duration = 1000
            interpolator = AccelerateInterpolator()
            start()
        }
    }

    private fun fade() {
        ObjectAnimator.ofFloat(image, View.ALPHA, 0.2f, 2.0f).apply {
            duration = 1000
            start()
        }

    }

    private fun scale() {
        val sX = ObjectAnimator.ofFloat(image, View.SCALE_X, 0.2f, 1.0f)
        val sY = ObjectAnimator.ofFloat(image, View.SCALE_Y, 0.2f, 1.0f)
        AnimatorSet().apply {
            playTogether(sX, sY)
            interpolator = AccelerateInterpolator()
            start()
        }
    }

    private fun combineAll() {

        val objectOne = AnimatorSet()
        val sX = ObjectAnimator.ofFloat(image, View.SCALE_X, 0.2f, 1.0f)
        val sY = ObjectAnimator.ofFloat(image, View.SCALE_Y, 0.2f, 1.0f)
        val fade = ObjectAnimator.ofFloat(image, View.ALPHA, 0.2f, 1.0f)

        objectOne.playTogether(sX, sY, fade)
        objectOne.duration = 600

        val objectTwo = AnimatorSet()

        val tx1 = ObjectAnimator.ofFloat(image, View.TRANSLATION_Y, 0f, 200f)
        tx1.duration = 1000
        tx1.interpolator = BounceInterpolator()

        val rotate = ObjectAnimator.ofFloat(image, View.ROTATION, -360f, 0f)
        rotate.duration = 1000
        rotate.interpolator = AccelerateInterpolator()

        objectTwo.playTogether(tx1, rotate)

        val finalObject = AnimatorSet()

        finalObject.play(objectOne).before(objectTwo)
        finalObject.play(objectTwo)

        finalObject.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {
            }

            override fun onAnimationCancel(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
                Toast.makeText(applicationContext, "Animation Fininsed", Toast.LENGTH_SHORT).show()
            }

            override fun onAnimationStart(p0: Animator?) {
                Toast.makeText(applicationContext, "Animation Started", Toast.LENGTH_SHORT).show()
            }

        })
        finalObject.start()
    }

    private fun valueAnim() {
        val mDuration = 1000 //in millis
        ValueAnimator.ofFloat(200f, 0f).apply {
            duration = mDuration.toLong()
            addUpdateListener { animation -> image!!.translationX = animation.animatedValue as Float }
            start()
        }
    }
}